import React from 'react'
import Router from 'next/router'
import useForm, { FormContext } from 'react-hook-form'
import { TagsInput } from './tags-input'
import { FormValue } from '../../typings/form'
import { useAddBookmark } from '../../helpers/bookmark'
import { ErrorMessage } from '../shared/error-message'

const Form = () => {
  const methods = useForm<FormValue>({
    defaultValues: {
      url: '',
      title: '',
      description: '',
      tags: [],
    },
  })
  const { register, handleSubmit } = methods
  const [addBookmark, isAdded, error] = useAddBookmark()

  const onSubmit: Parameters<typeof handleSubmit>[0] = data => {
    addBookmark(data)
  }

  if (isAdded) {
    Router.push('/bookmarks')
    return null
  }

  return (
    <FormContext {...methods}>
      <form onSubmit={handleSubmit(onSubmit)} className="mt-6">
        <div>
          <label
            htmlFor="url"
            className="text-sm uppercase tracking-wide block font-semibold mb-1"
          >
            URL
          </label>
          <input
            type="url"
            name="url"
            id="url"
            className="border border-gray-300 px-4 py-3 w-1/2"
            required
            ref={register}
          />
        </div>
        <div className="mt-4">
          <label
            htmlFor="title"
            className="text-sm uppercase tracking-wide block font-semibold mb-1"
          >
            Title
          </label>
          <input
            type="text"
            name="title"
            id="title"
            className="border border-gray-300 px-4 py-3 w-1/2"
            ref={register}
          />
        </div>
        <div className="mt-4">
          <label
            htmlFor="description"
            className="text-sm uppercase tracking-wide block font-semibold mb-1"
          >
            Description
          </label>
          <textarea
            name="description"
            id="description"
            className="border border-gray-300 px-4 py-3 w-1/2"
            rows={4}
            ref={register}
          />
        </div>
        <div className="mt-4">
          <label
            htmlFor="tags"
            className="text-sm uppercase tracking-wide block font-semibold mb-1"
          >
            Tags
          </label>
          {/* <input
          type="text"
          name="tags"
          id="tags"
          className="border border-gray-300 px-4 py-3 w-1/2"
        /> */}
          <TagsInput />
        </div>

        <div className="mt-4">
          <button className="btn btn-blue" type="submit">
            Submit
          </button>
        </div>
      </form>
      <ErrorMessage error={error} />
    </FormContext>
  )
}

export { Form }
