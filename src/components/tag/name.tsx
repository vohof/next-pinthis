import React from 'react'
import { TagDocument } from '../../typings/tag'

const Name: React.FC<{ tag: TagDocument }> = ({ tag }) => {
  return <h1 className="text-3xl mr-4">{tag.name}</h1>
}

export { Name }
