import Head from 'next/head'
import Link from 'next/link'
import React from 'react'
import { Nav } from '../components/shared/nav'
import { useAuthState } from '../helpers/auth'
import '../styles/styles.css'

const Home = () => {
  const [user] = useAuthState()

  return (
    <div>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="manifest" href="site.webmanifest" />
      </Head>
      <Nav marginBottom={0} user={user} />

      <div className="container mx-auto relative">
        <div className="mt-24 text-center">
          <div className="text-4xl font-medium">
            Bookmark links and organize them
          </div>
          <div
            className="mt-10 border border-gray-100 mx-auto bg-white shadow"
            style={{
              width: 800,
              height: 400,
            }}
          />
        </div>

        <div className="mt-10 text-center">
          <Link href="/signin">
            <a className="shadow-lg btn btn-lg btn-blue">Start Bookmarking!</a>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default Home
