import firebase from 'firebase/app'
import { useEffect, useReducer } from 'react'
import { useAuthState as useFirebaseAuthState } from 'react-firebase-hooks/auth'

type UseAuthState = {
  error: Error | undefined
  provider: firebase.auth.AuthProvider | undefined
}

type UseAuthAction =
  | {
      type: 'setError'
      error: Error
    }
  | {
      type: 'setProvider'
      provider: firebase.auth.AuthProvider
    }

const useAuthLogin = () => {
  const [{ error, provider }, dispatch] = useReducer(
    (state: UseAuthState, action: UseAuthAction) => {
      switch (action.type) {
        case 'setError':
          return {
            ...state,
            error: action.error,
          }
        case 'setProvider':
          return {
            ...state,
            provider: action.provider,
          }

        default:
          return state
      }
    },
    {
      error: undefined,
      provider: undefined,
    }
  )

  const login = (providerName: 'github' | 'google' | 'twitter') => {
    let providerInstance: firebase.auth.AuthProvider
    switch (providerName) {
      case 'github':
        providerInstance = new firebase.auth.GithubAuthProvider()
        break
      case 'google':
        providerInstance = new firebase.auth.GoogleAuthProvider()
        break
      case 'twitter':
        providerInstance = new firebase.auth.TwitterAuthProvider()
        break
      default:
        throw new Error(`Unknown provider: ${provider}`)
    }

    dispatch({
      type: 'setProvider',
      provider: providerInstance,
    })
  }

  useEffect(() => {
    if (!provider) {
      return
    }
    let mounted = true
    firebase
      .auth()
      .signInWithPopup(provider)
      .catch(error => {
        mounted && dispatch({ type: 'setError', error })
      })

    return () => {
      mounted = false
    }
  }, [provider])

  return [login, error] as const
}

const useAuthLogout = () => {
  const logout = () => {
    return firebase.auth().signOut()
  }

  return [logout]
}

const useAuthState = (): ReturnType<typeof useFirebaseAuthState> => {
  if (typeof window === 'undefined') {
    return [undefined, true, undefined]
  }

  return useFirebaseAuthState(firebase.auth())
}

export { useAuthLogin, useAuthLogout, useAuthState }
