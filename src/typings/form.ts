export type TagFormValue = {
  name: string
  id?: string
}

export type FormValue = {
  title: string
  url: string
  description: string
  tags: TagFormValue[]
}
