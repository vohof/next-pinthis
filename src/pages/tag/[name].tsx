import Head from 'next/head'
import Link from 'next/link'
import Router, { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { BookmarkList } from '../../components/home/bookmark-list'
import { ErrorBoundary } from '../../components/shared/error-boundary'
import { ErrorMessage } from '../../components/shared/error-message'
import { Nav } from '../../components/shared/nav'
import { Name } from '../../components/tag/name'
import { useAuthState } from '../../helpers/auth'
import { useTag } from '../../helpers/tag'
import ChevronIcon from '../../images/chevron.svg'
import PlusIcon from '../../images/plus.svg'

const Tag = () => {
  const [user, loading] = useAuthState()
  const router = useRouter()
  const { name } = router.query as { name: string }
  const [tag, error] = useTag(name)

  useEffect(() => {
    if (!user && !loading) {
      Router.push('/signin')
    }
  }, [user, loading])

  if (error) {
    return (
      <div className="container">
        <ErrorMessage error={error} />
      </div>
    )
  }

  if (!user || !tag) {
    return null
  }

  return (
    <div>
      <Head>
        <title>Bookmarks</title>
      </Head>
      <Nav user={user} />
      <div className="container mx-auto">
        <div className="inline-block">
          <Link href="/add">
            <a className="btn btn-blue flex items-center">
              <PlusIcon className="w-6 h-6 fill-current mr-4" /> Add Bookmark
            </a>
          </Link>
        </div>

        <ErrorBoundary>
          <div className="flex items-center mt-6">
            <h1 className="text-3xl mr-4">
              <Link href="/bookmarks">
                <a>Bookmarks</a>
              </Link>
            </h1>
            <span className="text-gray-200 mr-4">
              <ChevronIcon className="w-6 h-6 fill-current chevron chevron-right" />
            </span>
            <Name tag={tag} />
          </div>
          <BookmarkList user={user} tag={tag} />
        </ErrorBoundary>
      </div>
    </div>
  )
}

export default Tag
