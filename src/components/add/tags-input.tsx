import React, { useState, useEffect } from 'react'
import { useFormContext } from 'react-hook-form'
import { Input } from './input'
import { TagFormValue } from '../../typings/form'

const TagsInput = () => {
  const { register, setValue } = useFormContext()
  const [tags, setTags] = useState<TagFormValue[]>([])
  const removeTag = (index: number) => {
    setTags(tags => tags.filter((_, i) => i !== index))
  }

  useEffect(() => {
    register({ name: 'tags' })
    setValue('tags', tags)
  }, [tags])

  return (
    <div>
      <div className="w-1/2 flex flex-wrap items-center relative overflow-hidden">
        <Input
          type="text"
          placeholder={`Type a tag. Press "enter" to add.`}
          className="w-full px-2 border border-gray-300 py-3 px-6"
          style={{
            minWidth: '10rem',
          }}
          handleEnter={tag => {
            setTags(tags => [...tags, { name: tag }])
          }}
        />

        <ul
          className="absolute left-0 context-menu"
          style={{
            top: '100%',
          }}
        >
          <li>
            <a className="context-menu-item" href="#">
              Ipsum
            </a>
          </li>
          <li>
            <a className="context-menu-item" href="#">
              Dolor
            </a>
          </li>
        </ul>
      </div>

      <div className="tag-group w-1/2 mt-4">
        {tags.map((tag, index) => {
          return (
            <React.Fragment key={index}>
              <button
                className="tag"
                onClick={() => removeTag(index)}
                type="button"
              >
                {tag.name}
              </button>
            </React.Fragment>
          )
        })}
      </div>
    </div>
  )
}

export { TagsInput }
