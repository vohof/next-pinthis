import React from 'react'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import '../../public/empty.css'
import 'tailwindcss/base.css'
import 'tailwindcss/components.css'
import '../styles/styles.css'
import 'tailwindcss/utilities.css'

if (typeof window !== 'undefined') {
  firebase.initializeApp({
    apiKey: 'AIzaSyA9OGnXdsIa9z-JUWhEOsXn_-QQg5DMiiI',
    authDomain: 'pinthis-8e6c3.firebaseapp.com',
    databaseURL: 'https://pinthis-8e6c3.firebaseio.com',
    projectId: 'pinthis-8e6c3',
    storageBucket: 'pinthis-8e6c3.appspot.com',
    messagingSenderId: '194578694947',
    appId: '1:194578694947:web:5d4838fec20e1128f34296',
  })
}

/**
 * Create a page which wraps the Auth0 provider.
 */
const App: React.FC<{ Component: React.FC; props: any }> = ({
  Component,
  props,
}) => <Component {...props} />

export default App
