export type BookmarkDocumentTags = Record<string, { name: string }>

export type BookmarkDocument = {
  title: string
  url: string
  description: string
  tags: BookmarkDocumentTags
}
