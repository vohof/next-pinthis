module.exports = ({ file, options, env }) => {
  return {
    plugins: {
      'postcss-import': {},
      tailwindcss: './tailwind.config.js',
      'postcss-preset-env': {
        features: {
          'nesting-rules': true,
        },
      },
      cssnano: env === 'production' ? {} : false,
    },
  }
}
