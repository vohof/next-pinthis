import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

admin.initializeApp(functions.config().firebase)

const db = admin.firestore()

type BookmarkDocumentTags = {
  id: string
  name: string
}

type BookmarkDocument = {
  uid: string
  title: string
  url: string
  description: string
  tags: BookmarkDocumentTags[]
}

export const addBookmark = functions.firestore
  .document('bookmarks/{bookmarkId}')
  .onCreate(async (snapshot, context) => {
    const data = snapshot.data() as BookmarkDocument

    await Promise.all(
      data.tags.map(async tag => {
        const doc = db.collection('tags').doc(tag.id)
        doc.update({
          bookmarkIds: admin.firestore.FieldValue.arrayUnion(snapshot.id),
        })
      })
    )
  })

// TODO: remove tag
