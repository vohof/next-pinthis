import React from 'react'
import { ErrorMessage } from './error-message'

class ErrorBoundary extends React.Component {
  state: { error: Error | undefined } = { error: undefined }

  static getDerivedStateFromError(error: Error) {
    return { error }
  }

  render() {
    const { error } = this.state

    return <>{error ? <ErrorMessage error={error} /> : this.props.children}</>
  }
}

export { ErrorBoundary }
