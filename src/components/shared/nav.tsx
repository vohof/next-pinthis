import Link from 'next/link'
import React from 'react'
import { useAuthLogout } from '../../helpers/auth'
import Logo from '../../images/logo.svg'

const Nav: React.FC<{
  marginBottom?: number
  user?: firebase.User | undefined
}> = ({ marginBottom = 10, user }) => {
  const [logout] = useAuthLogout()

  return (
    <div className={`shadow overflow-hidden mb-${marginBottom} bg-white`}>
      <div className="container mx-auto px-4">
        <div className="my-4 flex items-center">
          <Link href="/">
            <a>
              <Logo className="h-12" />
            </a>
          </Link>
          {user && (
            <ul className="ml-16 navmenu">
              <li className="navmenu-item">
                <Link href="/bookmarks">
                  <a className="navmenu-link">Bookmarks</a>
                </Link>
              </li>
            </ul>
          )}
          <ul className="ml-auto navmenu">
            <li className="navmenu-item">
              {user ? (
                <button className="navmenu-link" onClick={logout}>
                  Sign Out
                </button>
              ) : (
                <Link href="/signin">
                  <a className="navmenu-link">Sign In</a>
                </Link>
              )}
            </li>
            <li>
              <a className="navmenu-link" href="#">
                Github
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export { Nav }
