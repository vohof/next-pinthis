import Link from 'next/link'
import React from 'react'
import useSWR from 'swr'
import { listBookmarks } from '../../helpers/bookmark'
import { BookmarkDocument } from '../../typings/bookmark'
import { TagDocument } from '../../typings/tag'

type props = {
  tag?: TagDocument
  user: firebase.User | undefined
}

const BookmarkList: React.FC<props> = ({ tag, user }) => {
  const { data: bookmarks, error } = useSWR<BookmarkDocument[]>(
    [user, tag],
    listBookmarks
  )

  if (error) {
    throw error
  }

  if (typeof bookmarks === 'undefined' || !bookmarks.length) {
    return <div>No bookmarks yet</div>
  }

  return (
    <div>
      {bookmarks.map(bookmark => {
        return (
          <div className="flex mb-4" key={bookmark.url}>
            <div>
              <a href={bookmark.url} target="_blank" rel="noopener noreferrer">
                <div
                  style={{
                    width: '8rem',
                    height: '5rem',
                    background: 'rgba(0,0,0,.1)',
                  }}
                ></div>
              </a>
            </div>
            <div className="ml-4 leading-none">
              <h3>
                <a
                  href={bookmark.url}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="text-blue-500 text-lg"
                >
                  {bookmark.title}
                </a>
              </h3>
              <a
                href={bookmark.url}
                target="_blank"
                rel="noopener noreferrer"
                className="text-gray-500 text-sm tracking-tight"
              >
                {bookmark.url}
              </a>
              <p className="leading-tight mt-2">{bookmark.description}</p>
              <ul className="navmenu text-sm mt-1">
                {Object.keys(bookmark.tags).map(tag => (
                  <li key={tag}>
                    <Link href={`/tag/[name]`} as={`/tag/${tag}`}>
                      <a className="navmenu-link">{bookmark.tags[tag].name}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export { BookmarkList }
