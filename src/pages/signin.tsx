import React from 'react'
import Head from 'next/head'
import Router from 'next/router'
import { Nav } from '../components/shared/nav'
import Google from '../images/google.svg'
import Github from '../images/github.svg'
import Twitter from '../images/twitter.svg'
import { useAuthLogin, useAuthState } from '../helpers/auth'
import { ErrorMessage } from '../components/shared/error-message'

const Signin = () => {
  const [login, error] = useAuthLogin()
  const [user] = useAuthState()

  if (user) {
    Router.replace('/bookmarks')
    return null
  }

  return (
    <div>
      <Head>
        <title>Sign In</title>
      </Head>
      <Nav user={user} />

      <div className="container mx-auto">
        <h1 className="text-2xl font-semibold mb-10">Sign In using...</h1>

        <div className="flex btn-group">
          <button
            className="btn btn-alternate btn-gray flex items-center justify-center"
            onClick={() => login('github')}
          >
            <Github className="w-10 mr-4" style={{ fill: '#181717' }} />
            Github
          </button>
          <button
            className="btn btn-alternate btn-gray flex items-center justify-center"
            onClick={() => login('google')}
          >
            <Google style={{ fill: '#4285F4' }} className="w-10 mr-4" />
            Google
          </button>
          <button
            className="btn btn-alternate btn-gray flex items-center justify-center"
            onClick={() => login('twitter')}
          >
            <Twitter style={{ fill: '#1DA1F2' }} className="w-10 mr-4" />
            Twitter
          </button>
        </div>

        {error && <ErrorMessage error={error} />}
      </div>
    </div>
  )
}

export default Signin
