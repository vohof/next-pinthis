import firebase from 'firebase/app'
import { useEffect, useState } from 'react'
import * as Collection from '../constants/collections'
import { BookmarkDocument } from '../typings/bookmark'
import { FormValue } from '../typings/form'
import { TagDocument } from '../typings/tag'
import { useAuthState } from './auth'
import { findOrCreateTag } from './tag'

const useAddBookmark = () => {
  const [isAdded, setIsAdded] = useState(false)
  const [error, setError] = useState<Error>()
  const [bookmark, setBookmark] = useState<FormValue>()
  const [user] = useAuthState()

  useEffect(() => {
    let mounted = true
    if (user && bookmark) {
      findOrCreateTag(user, bookmark.tags)
        .then(tags => {
          firebase
            .firestore()
            .collection(Collection.Bookmarks)
            .add({ ...bookmark, uid: user.uid, tags })
            .then(() => mounted && setIsAdded(true))
            .catch(error => mounted && setError(error))
        })
        .catch(error => mounted && setError(error))
    }

    return () => {
      mounted = false
    }
  }, [user, bookmark])

  return [setBookmark, isAdded, error] as const
}

const listBookmarks = async (
  user?: firebase.User,
  tag?: TagDocument
): Promise<BookmarkDocument[]> => {
  if (!user) {
    return []
  }

  let query: firebase.firestore.Query = firebase
    .firestore()
    .collection(Collection.Bookmarks)
    .where('uid', '==', user.uid)

  if (tag) {
    // query = query.where('')
  }

  const qs = await query.get()

  const bookmarks = qs.docs.map(ds => ds.data() as BookmarkDocument)

  return bookmarks
}

const useListBookmarks = () => {
  const [bookmarks, setBookmarks] = useState<BookmarkDocument[]>([])
  const [error, setError] = useState<Error>()
  const [user] = useAuthState()

  useEffect(() => {
    let mounted = true
    if (user) {
      firebase
        .firestore()
        .collection(Collection.Bookmarks)
        .where('uid', '==', user.uid)
        .get()
        .then(qs => {
          const bookmarks = qs.docs.map(ds => ds.data())
          mounted && setBookmarks(bookmarks as BookmarkDocument[])
        })
        .catch(setError)
    }

    return () => {
      mounted = false
    }
  }, [user])

  return [bookmarks, error] as const
}

export { useAddBookmark, listBookmarks }
