import React from 'react'

const ErrorMessage: React.FC<{ error?: Error }> = ({ error }) => {
  if (!error) {
    return null
  }
  return (
    <div className="py-4 border border-red-300 px-6 text-red-400">
      Error: {error.message}
    </div>
  )
}

export { ErrorMessage }
