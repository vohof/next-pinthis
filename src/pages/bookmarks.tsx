import { NextPage } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import Router from 'next/router'
import React, { useEffect } from 'react'
import { BookmarkList } from '../components/home/bookmark-list'
import { ErrorBoundary } from '../components/shared/error-boundary'
import { Nav } from '../components/shared/nav'
import { useAuthState } from '../helpers/auth'
import PlusIcon from '../images/plus.svg'

const Bookmarks: NextPage = () => {
  const [user, loading] = useAuthState()

  useEffect(() => {
    if (!user && !loading) {
      Router.push('/signin')
    }
  }, [user, loading])

  if (!user) {
    return null
  }

  return (
    <div>
      <Head>
        <title>Bookmarks</title>
      </Head>
      <Nav user={user} />
      <div className="container mx-auto">
        <div className="flex mb-4">
          <div className="flex items-center">
            <h1 className="text-3xl mr-4 font-thin">
              <Link href="/bookmarks">
                <a>Bookmarks</a>
              </Link>
            </h1>
            <Link href="/add">
              <a className="btn btn-blue flex items-center mb-0">
                <PlusIcon className="w-4 h-4 fill-current mr-2" /> Add Bookmark
              </a>
            </Link>
          </div>
          <div className="ml-auto flex items-center">
            <input
              type="text"
              className="border border-gray-300 px-4 py-1 rounded"
              placeholder="Type a tag"
            />
          </div>
        </div>
      </div>
      <div className="container mx-auto bg-white border border-gray-200 px-4 pb-2 pt-6">
        <ErrorBoundary>
          <BookmarkList user={user} />
        </ErrorBoundary>
      </div>
    </div>
  )
}

Bookmarks.getInitialProps = async () => {
  return {
    protected: true,
  }
}

export default Bookmarks
