const withCSS = require('@zeit/next-css')

module.exports = withCSS({
  webpack(config, options) {
    config.module.rules.push({
      test: /\.svg$/,
      exclude: /node_modules/,
      use: '@svgr/webpack',
    })
    return config
  },
})
