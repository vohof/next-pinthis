import firebase, { firestore } from 'firebase/app'
import { useEffect, useState } from 'react'
import { BookmarkDocumentTags } from '../typings/bookmark'
import { TagDocument } from '../typings/tag'
import { useAuthState } from './auth'

const useAllTags = () => {
  const [user] = useAuthState()
  const [allTags, setAllTags] = useState<any[]>([])
  const [error, setError] = useState<Error>()

  useEffect(() => {
    let mounted = true
    if (user) {
      firebase
        .firestore()
        .collectionGroup('tags')
        .get()
        .then(qs => {
          const tags = qs.docs.map(ds => ds.data())
          mounted && setAllTags(tags)
        })
        .catch(error => mounted && setError(error))
    }

    return () => {
      mounted = false
    }
  }, [user])

  return [allTags, error] as const
}

const useTag = (id: string) => {
  const [tag, setTag] = useState<TagDocument>()
  const [error, setError] = useState<Error>()

  useEffect(() => {
    let mounted = true

    firebase
      .firestore()
      .collection('tags')
      .doc(id)
      .get()
      .then(docSnapshot => {
        if (!docSnapshot.exists) {
          mounted && setError(new Error('Tag does not exist.'))
        }

        setTag(docSnapshot.data() as TagDocument)
      })

    return () => {
      mounted = false
    }
  }, [id])

  return [tag, error] as const
}

const useTagSuggestion = (tag: string) => {
  // const fuse = new Fuse()
}

const addTag = async (user: firebase.User, tag: string) => {
  return await firebase
    .firestore()
    .collection('tags')
    .add({ name: tag, uid: user.uid })
}

const findOrCreateTag = async (
  user: firebase.User,
  tags: Array<{ name: string; id?: string }>
): Promise<BookmarkDocumentTags> => {
  const tagRefs: Record<string, { name: string }> = {}

  await Promise.all(
    tags.map(async ({ name, id }) => {
      let docReference: firestore.DocumentReference

      if (typeof id === 'undefined') {
        docReference = await addTag(user, name)
      } else {
        docReference = firebase
          .firestore()
          .collection('tags')
          .doc(id)

        if (!(await docReference.get()).exists) {
          docReference = await addTag(user, name)
        }
      }

      tagRefs[docReference.id] = { name }
    })
  )

  return tagRefs
}

export { useAllTags, useTag, useTagSuggestion, findOrCreateTag }
