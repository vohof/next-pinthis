import React, { useEffect } from 'react'
import Router from 'next/router'
import Head from 'next/head'
import Link from 'next/link'
import { useAuthState } from '../helpers/auth'
import { Nav } from '../components/shared/nav'
import ChevronIcon from '../images/chevron.svg'
import { ErrorBoundary } from '../components/shared/error-boundary'
import { Form } from '../components/add/form'

const Add = () => {
  const [user, loading] = useAuthState()

  useEffect(() => {
    if (!user && !loading) {
      Router.push('/signin')
    }
  }, [user, loading])

  if (!user) {
    return null
  }

  return (
    <div>
      <Head>
        <title>Add Bookmark</title>
      </Head>
      <Nav user={user} />
      <div className="container mx-auto">
        <div className="mt-12">
          <Link href="/bookmarks">
            <a className="flex items-center uppercase text-blue-400 font-normal">
              <ChevronIcon className="w-4 h-4 fill-current mr-1" />
              Back
            </a>
          </Link>
        </div>
        <h1 className="text-3xl mt-14">Add Bookmark</h1>

        <ErrorBoundary>
          <Form />
        </ErrorBoundary>
      </div>
    </div>
  )
}

export default Add
