export type TagDocument = {
  name: string
  bookmarkIds: string[]
}
