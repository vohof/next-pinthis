import React, { useState } from 'react'

type props = {
  handleEnter?: (text: string) => void
} & React.InputHTMLAttributes<HTMLInputElement>

const Input: React.FC<props> = ({
  placeholder,
  className,
  type,
  style,
  handleEnter,
}) => {
  const [inputValue, setInputValue] = useState('')

  return (
    <input
      type={type}
      value={inputValue}
      onChange={e => {
        setInputValue(e.target.value)
      }}
      onKeyDown={e => {
        if (e.keyCode === 13 && typeof handleEnter === 'function') {
          e.preventDefault()
          handleEnter(inputValue)
          setInputValue('')
        }
      }}
      placeholder={placeholder}
      className={className}
      style={style}
    />
  )
}

export { Input }
